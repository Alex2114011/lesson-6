//
//  NormalNavigation.swift
//  Navigations
//
//  Created by Alex on 13.03.2022.
//

import UIKit

final class NormalNavigation: UIViewController {

    // MARK: - Private property

    private lazy var openVCButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.backgroundColor = .systemBlue
        button.setTitle("Open VC", for: .normal)
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(didTapOpenButton), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(openVCButton)
        setupUI()
    }
}

// MARK: - Private methods

extension NormalNavigation {

    private func setupUI() {
        view.backgroundColor = .white

        NSLayoutConstraint.activate([openVCButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     openVCButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     openVCButton.widthAnchor.constraint(equalToConstant: view.frame.width / 2)])
    }


    @objc private func didTapOpenButton() {
        navigationController?.pushViewController(NewViewController(), animated: true)
    }
}
