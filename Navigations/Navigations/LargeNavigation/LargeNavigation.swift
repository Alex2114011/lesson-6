//
//  LargeNavigation.swift
//  Navigations
//
//  Created by Alex on 13.03.2022.
//

import UIKit

final class LargeNavigation: UIViewController {

    // MARK: - Private property

    private lazy var openNextButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.backgroundColor = .systemBlue
        button.setTitle("Open next", for: .normal)
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(didTapOpenButton), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(openNextButton)
        setupUI()
    }
}

// MARK: - Private methods

extension LargeNavigation {

    private func setupUI() {
        view.backgroundColor = .systemBackground

        NSLayoutConstraint.activate([openNextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     openNextButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     openNextButton.widthAnchor.constraint(equalToConstant: view.frame.width / 2)])
    }


    @objc private func didTapOpenButton() {
        navigationController?.pushViewController(NewViewController(), animated: true)
    }
}
