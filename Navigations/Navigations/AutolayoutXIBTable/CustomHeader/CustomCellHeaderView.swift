//
//  CustomCellHeaderView.swift
//  Navigations
//
//  Created by Alex on 07.03.2022.
//

import UIKit

final class CustomCellHeaderView: UITableViewHeaderFooterView {
    
    // MARK: - Private property
    
    @IBOutlet weak private var logoImageView: UIImageView!
    @IBOutlet weak private var headerTitle: UILabel!
    
    // MARK: - Public methods
    
    public func configureHeaderView(header text: String, and image: UIImage?) {
        headerTitle.text = text
        logoImageView.image = image
    }
    
}
