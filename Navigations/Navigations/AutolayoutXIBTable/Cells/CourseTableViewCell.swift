//
//  CourseTableViewCell.swift
//  Navigations
//
//  Created by Alex on 07.03.2022.
//

import UIKit

final class CourseTableViewCell: UITableViewCell {

    // MARK: - Callback

    public var didTapMoreButtonClosure:(() -> ())?
    public var didTapAddButtonClosure:(() -> ())?

    // MARK: - Private property

    @IBOutlet weak private var advertisingView: CustomXIBView!
    @IBOutlet weak private var advertisingLabel: UILabel!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var priceLabel: UILabel!

    // MARK: - Private constraints

    @IBOutlet weak private var advertiseViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak private var advertiseLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var advertiseLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak private var advertiseLabelBottomConstraint: NSLayoutConstraint!

    // MARK: - Public methods

    public func configureCell(with model: CourseModel) {
        if model.advertisingText != nil {
            advertisingLabel.text = model.advertisingText
        } else {
            advertiseViewTopConstraint.constant = 0
            advertiseLabelTopConstraint.constant = 0
            advertiseLabelBottomConstraint.constant = 0
            advertiseLabelHeightConstraint.constant = 0
        }
        nameLabel.text = model.nameCourse
        priceLabel.text = model.priceCourse
    }
}

// MARK: - Private methods

extension CourseTableViewCell {

    @IBAction private func didTapMoreButton(_ sender: UIButton) {
        didTapMoreButtonClosure?()
    }

    @IBAction private func didTapAddButton(_ sender: UIButton) {
        didTapAddButtonClosure?()
    }
}
