//
//  CourseViewController.swift
//  Navigations
//
//  Created by Alex on 07.03.2022.
//

import UIKit

final class CourseViewController: UIViewController {

    // MARK: - Private Property

    @IBOutlet weak private var courseTableView: UITableView!

    private var courses:[CourseModel] = [CourseModel(advertisingText: "Первые три занятия беcплатно",
                                                     nameCourse: "Backend разработка",
                                                     priceCourse: "7 840"),
                                         CourseModel(advertisingText: "1.02.2022 - 1.04.2022 Скидка 10%",
                                                     nameCourse: "Курсы по Android разроботке",
                                                     priceCourse: "9 800"),
                                         CourseModel(advertisingText: nil, nameCourse: "Курсы по iOS разработке + Computer Science + Computer Vision + System Design",
                                                     priceCourse: "11 760")]


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(named: "appBackround")
        configureTableView()
        configureNavBar()
    }
}

// MARK: - Private Methods

extension CourseViewController {

    private func configureNavBar() {
        let item = UIBarButtonItem(barButtonSystemItem: .compose,
                                   target: self,
                                   action: #selector(didTapCompose))
        item.tintColor = .white
        navigationItem.rightBarButtonItem = item

    }

    private func configureTableView() {
        courseTableView.separatorStyle = .none
        courseTableView.backgroundColor = UIColor(named: "appBackround")
        courseTableView.register(UINib(nibName: String(describing: CourseTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CourseTableViewCell.self))
        courseTableView.register(UINib(nibName: String(describing: CustomCellHeaderView.self), bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: CustomCellHeaderView.self))
    }

    @objc private func didTapCompose(){
        navigationController?.present(NormalNavigation(), animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource

extension CourseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = courses[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CourseTableViewCell.self), for: indexPath) as! CourseTableViewCell
        cell.configureCell(with: model)
        cell.didTapAddButtonClosure = { [weak self] in
            guard let self = self else { return }
            self.tabBarController?.selectedIndex = 3
        }
        cell.didTapMoreButtonClosure = { [weak self] in
            guard let self = self else { return }
            self.navigationController?.pushViewController(NewViewController(), animated: true)
        }

        return cell
    }
}

// MARK: - UITableViewDelegate

extension CourseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: CustomCellHeaderView.self)) as! CustomCellHeaderView
        headerView.configureHeaderView(header: "Курсы IT", and: UIImage(named: "technicalSupport"))
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 58
    }
}
