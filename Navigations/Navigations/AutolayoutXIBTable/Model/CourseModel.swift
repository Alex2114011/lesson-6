//
//  CourseModel.swift
//  Navigations
//
//  Created by Alex on 07.03.2022.
//

import Foundation

struct CourseModel {
    let advertisingText: String?
    let nameCourse: String?
    let priceCourse: String?
}
