//
//  CardView.swift
//  Navigations
//
//  Created by Alex on 06.03.2022.
//

import UIKit

final class CustomCardView: UIView {

    // MARK: - Callback

    public var didTapMoreButtonClosure: (() -> ())?
    public var didTapAddButtonClosure: (() -> ())?

    // MARK: - Private Property

    private lazy var advertisingView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.init(red: 181/255, green:  181/255, blue:  185/255, alpha: 1).cgColor
        return view
    }()

    private lazy var advertisingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.init(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)
        label.font = .systemFont(ofSize: 13, weight: .regular)
        return label
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.init(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        return label
    }()

    private lazy var moreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Подробнее", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        button.tintColor = UIColor.init(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)
        button.addTarget(self, action: #selector(didTapMoreButton), for: .touchUpInside)
        return button
    }()

    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.init(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        return label
    }()

    private lazy var addButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.backgroundColor = UIColor.init(red: 18/255, green: 139/255, blue: 227/255, alpha: 1)
        let image = UIImage(named: "addImage")
        button.setImage(image, for: .normal)
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(didTapAddButton), for: .touchUpInside)
        return button
    }()

    // MARK: - Initialisation

    init(frame: CGRect, advertisingText: String?, nameText: String, price: String) {
        super.init(frame: frame)
        self.advertisingLabel.text = advertisingText
        self.nameLabel.text = nameText
        self.priceLabel.text = price + " ₽"
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private Methods

extension CustomCardView {
    private func configureUI() {
        self.backgroundColor = .systemBackground
        self.layer.cornerRadius = 12
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(red: 223/255, green: 222/255, blue: 226/255, alpha: 1).cgColor

        self.addSubview(nameLabel)
        self.addSubview(moreButton)
        self.addSubview(priceLabel)
        self.addSubview(addButton)

        if advertisingLabel.text != nil {
            self.addSubview(advertisingView)
            advertisingView.addSubview(advertisingLabel)
            NSLayoutConstraint.activate([
                advertisingLabel.topAnchor.constraint(equalTo: advertisingView.topAnchor, constant: 4),
                advertisingLabel.leadingAnchor.constraint(equalTo: advertisingView.leadingAnchor, constant: 12),
                advertisingLabel.trailingAnchor.constraint(equalTo: advertisingView.trailingAnchor, constant: -12),
                advertisingLabel.bottomAnchor.constraint(equalTo: advertisingView.bottomAnchor, constant: -4),

                advertisingView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
                advertisingView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                advertisingView.trailingAnchor.constraint(lessThanOrEqualTo: self.trailingAnchor, constant: -20),

                nameLabel.topAnchor.constraint(equalTo: advertisingView.bottomAnchor, constant: 12),
                nameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                nameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),

                moreButton.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
                moreButton.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor),

                priceLabel.topAnchor.constraint(equalTo: moreButton.bottomAnchor, constant: 23),
                priceLabel.leadingAnchor.constraint(equalTo: moreButton.leadingAnchor),

                addButton.topAnchor.constraint(equalTo: moreButton.bottomAnchor, constant: 16),
                addButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
                addButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
                addButton.widthAnchor.constraint(equalToConstant: 72),
                addButton.heightAnchor.constraint(equalToConstant: 36)
            ])
        } else {
            NSLayoutConstraint.activate([
                nameLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
                nameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                nameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),

                moreButton.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
                moreButton.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor),

                priceLabel.topAnchor.constraint(equalTo: moreButton.bottomAnchor, constant: 23),
                priceLabel.leadingAnchor.constraint(equalTo: moreButton.leadingAnchor),

                addButton.topAnchor.constraint(equalTo: moreButton.bottomAnchor, constant: 16),
                addButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
                addButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
                addButton.widthAnchor.constraint(equalToConstant: 72),
                addButton.heightAnchor.constraint(equalToConstant: 36)
            ])
        }
    }
    @objc private func didTapMoreButton() {
        didTapMoreButtonClosure?()
    }

    @objc private func didTapAddButton() {
        didTapAddButtonClosure?()
    }
}
