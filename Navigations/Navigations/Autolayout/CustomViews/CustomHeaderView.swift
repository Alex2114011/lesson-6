//
//  CustomHeaderView.swift
//  Navigations
//
//  Created by Alex on 06.03.2022.
//

import UIKit

final class CustomHeaderView: UIView {

    // MARK: - Private Property
    
    private lazy var logoImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var titleLable: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Курсы IT"
        label.font = .systemFont(ofSize: 17, weight: .medium)
        return label
    }()

    // MARK: - Initialisation

    init(frame: CGRect, titleText: String, imageName: String) {
        super.init(frame: frame)
        self.titleLable.text = titleText
        self.logoImageView.image = UIImage(named: imageName)
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private Methods

extension CustomHeaderView {
    private func configureUI() {
        self.addSubview(logoImageView)
        self.addSubview(titleLable)
        NSLayoutConstraint.activate([logoImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     logoImageView.topAnchor.constraint(equalTo: self.topAnchor),
                                     logoImageView.heightAnchor.constraint(equalToConstant: 40),
                                     logoImageView.widthAnchor.constraint(equalTo: logoImageView.heightAnchor),

                                     titleLable.topAnchor.constraint(equalTo: logoImageView.topAnchor),
                                     titleLable.leadingAnchor.constraint(equalTo: logoImageView.trailingAnchor,constant: 12),
                                     titleLable.heightAnchor.constraint(equalToConstant: 22),
                                     titleLable.trailingAnchor.constraint(equalTo: self.trailingAnchor)
                                    ])
    }
}
