//
//  AutolayoutViewController.swift
//  Navigations
//
//  Created by Alex on 06.03.2022.
//

import UIKit

final class AutolayoutViewController: UIViewController {

    // MARK: - Private Property

    private let autolayoutView: AutolayoutView = {
        let view = AutolayoutView()
        return view
    }()

    override func loadView() {
        view = autolayoutView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavBar()
        didTapMoreButton()
        didTapAddButton()
    }
}

// MARK: - Private Methods

extension AutolayoutViewController {
    private func configureNavBar() {
        let item = UIBarButtonItem(
            barButtonSystemItem: .camera,
            target: self,
            action: #selector(didTapCamera))
        item.tintColor = .white
        navigationItem.rightBarButtonItem = item

    }

    @objc private func didTapCamera() {
        navigationController?.present(NormalNavigation(), animated: true, completion: nil)
        navigationController?.definesPresentationContext = true
        navigationController?.presentedViewController?.view.alpha = 0.5
    }

    private func didTapMoreButton() {
        let cards = [autolayoutView.firstCard, autolayoutView.secondCard, autolayoutView.thirdCard]

        cards.forEach { card in
            card.didTapMoreButtonClosure = {[weak self] in
                guard let self = self else { return }
                self.navigationController?.pushViewController(NewViewController(), animated: true)
            }
        }
    }

    private func didTapAddButton() {
        let cards = [autolayoutView.firstCard, autolayoutView.secondCard, autolayoutView.thirdCard]

        cards.forEach { card in
            card.didTapAddButtonClosure = { [weak self] in
                guard let self = self else { return }
                self.tabBarController?.selectedIndex = 3
            }
        }
    }
}

