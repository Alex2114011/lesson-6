//
//  AutolayoutView.swift
//  Navigations
//
//  Created by Alex on 04.03.2022.
//

import UIKit

final class AutolayoutView: UIView {

    // MARK: - Private Property

    private lazy var rootScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var headerView: CustomHeaderView = {
        let view = CustomHeaderView(frame: .zero, titleText: "Курсы IT", imageName: "technicalSupport")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: - Public Property

    lazy var firstCard: CustomCardView = {
        let view = CustomCardView(frame: .zero,
                                  advertisingText: "Первые три занятия беcплатно",
                                  nameText: "Backend разработка",
                                  price: "7 840")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var secondCard: CustomCardView = {
        let view = CustomCardView(frame: .zero,
                                  advertisingText: "1.02.2022 - 1.04.2022 Скидка 10%",
                                  nameText: "Курсы по Android разроботке",
                                  price: "9 800")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var thirdCard: CustomCardView = {
        let view = CustomCardView(frame: .zero,
                                  advertisingText: nil,
                                  nameText: "Курсы по iOS разработке + Computer Science + Computer Vision + System Design",
                                  price: "11 760")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: - Initialisation

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private Methods

extension AutolayoutView {

    private func configureUI() {
        self.backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)
        self.addSubview(rootScrollView)

        rootScrollView.addSubview(contentView)

        contentView.addSubview(headerView)
        contentView.addSubview(firstCard)
        contentView.addSubview(secondCard)
        contentView.addSubview(thirdCard)

        NSLayoutConstraint.activate([
            rootScrollView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            rootScrollView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
            rootScrollView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor),
            rootScrollView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),

            contentView.topAnchor.constraint(equalTo: rootScrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: rootScrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: rootScrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: rootScrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: rootScrollView.widthAnchor),

            headerView.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 9),
            headerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            headerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -16),
            headerView.heightAnchor.constraint(equalToConstant: 40),

            firstCard.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 25),
            firstCard.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            firstCard.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),

            secondCard.topAnchor.constraint(equalTo: firstCard.bottomAnchor, constant: 16),
            secondCard.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            secondCard.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),

            thirdCard.topAnchor.constraint(equalTo: secondCard.bottomAnchor, constant: 16),
            thirdCard.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            thirdCard.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            thirdCard.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

