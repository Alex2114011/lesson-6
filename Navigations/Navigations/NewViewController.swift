//
//  EmbedViewController.swift
//  Navigations
//
//  Created by Alex on 14.03.2022.
//

import UIKit

class NewViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemTeal
        navigationItem.largeTitleDisplayMode = .never
    }
}
