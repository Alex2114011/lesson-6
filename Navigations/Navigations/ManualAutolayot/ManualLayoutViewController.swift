//
//  ManualLayoutViewController.swift
//  Navigations
//
//  Created by Alex on 08.03.2022.
//

import UIKit

final class ManualLayoutViewController: UIViewController {

    // MARK: - Private property

   private let customView: ManualLayoutView = {
        let view = ManualLayoutView()
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(named: "appBackround")
        view.addSubview(customView)
        configureNavBar()
        didTapMoreButton()
        didTapAddButton()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        customView.frame = CGRect(x: 0, y: self.view.safeAreaInsets.top, width: self.view.frame.width, height: self.view.frame.height)
    }

//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        customView.frame = CGRect(x: 0, y: self.view.safeAreaInsets.top, width: self.view.frame.width, height: self.view.frame.height)
//    }
}

// MARK: - Private methods

extension ManualLayoutViewController {

    private func configureNavBar() {
        let item = UIBarButtonItem(barButtonSystemItem: .bookmarks,
                                   target: self,
                                   action: #selector(didTapBookmarks))
        item.tintColor = .white
        navigationItem.rightBarButtonItem = item

    }

    private func didTapMoreButton() {
        let cards = [customView.firstCard, customView.secondCard, customView.thirdCard]

        cards.forEach { card in
            card.didTapMoreButtonClosure = {[weak self] in
                guard let self = self else { return }
                self.navigationController?.pushViewController(NewViewController(), animated: true)
            }
        }
    }

    private func didTapAddButton() {
        let cards = [customView.firstCard, customView.secondCard, customView.thirdCard]

        cards.forEach { card in
            card.didTapAddButtonClosure = { [weak self] in
                guard let self = self else { return }
                self.tabBarController?.selectedIndex = 3
            }
        }
    }
    
    @objc private func didTapBookmarks(){
        let vc = NormalNavigation()
        let nav = UINavigationController(rootViewController: vc)
        navigationController?.present(nav, animated: true, completion: nil)
    }
}

