//
//  CustomHeaderViewManual.swift
//  Navigations
//
//  Created by Alex on 09.03.2022.
//

import UIKit

final class CustomHeaderViewManual: UIView {
    
    //MARK: - Private property
    
    private lazy var logoImageView: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    private lazy var titleLable: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 17, weight: .medium)
        return label
    }()
    
    //MARK: - Initialisation
    
    init(frame: CGRect, titleText: String, imageName: String) {
        super.init(frame: frame)
        self.titleLable.text = titleText
        self.logoImageView.image = UIImage(named: imageName)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Private methods

extension CustomHeaderViewManual {
    private func configureUI() {
        self.addSubview(logoImageView)
        self.addSubview(titleLable)
        
        titleLable.sizeToFit()
        
        logoImageView.frame = CGRect(
            x: 0,
            y: 9,
            width: 40,
            height: 40)
        
        titleLable.frame = CGRect(
            x: logoImageView.frame.maxX + 12,
            y: 9,
            width: self.bounds.width - 52,
            height: 22)
        
        self.bounds = CGRect(x: 0, y: 0 ,width: self.bounds.width, height: logoImageView.frame.maxY + 9)
    }
}
