//
//  CustomCardViewManual.swift
//  Navigations
//
//  Created by Alex on 08.03.2022.
//

import UIKit

final class CustomCardViewManual: UIView {

    // MARK: - Callback

    public var didTapMoreButtonClosure: (() -> ())?
    public var didTapAddButtonClosure: (() -> ())?

    // MARK: - Private Property
    private lazy var advertisingView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 12
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.init(red: 181/255, green:  181/255, blue:  185/255, alpha: 1).cgColor
        return view
    }()

    private lazy var advertisingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.init(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)
        label.font = .systemFont(ofSize: 13, weight: .regular)
        return label
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.init(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        return label
    }()

    private lazy var moreButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Подробнее", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        button.tintColor = UIColor.init(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)
        button.addTarget(self, action: #selector(didTapMoreButton), for: .touchUpInside)
        return button
    }()

    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.init(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        return label
    }()

    private lazy var addButton: UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .white
        button.backgroundColor = UIColor.init(red: 18/255, green: 139/255, blue: 227/255, alpha: 1)
        let image = UIImage(named: "addImage")
        button.setImage(image, for: .normal)
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(didTapAddButton), for: .touchUpInside)
        return button
    }()

    // MARK: - Initialisation

    init(frame: CGRect, advertisingText: String?, nameText: String, price: String) {
        super.init(frame: frame)
        self.advertisingLabel.text = advertisingText
        self.nameLabel.text = nameText
        self.priceLabel.text = price + " ₽"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Private methods

extension CustomCardViewManual {

    func configureUI() {
        self.backgroundColor = .white
        self.layer.cornerRadius = 12
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(red: 223/255, green: 222/255, blue: 226/255, alpha: 1).cgColor


        let labelHeight = heightForLable(text: nameLabel.text, font: nameLabel.font, width: self.bounds.width - 32)

        if advertisingLabel.text != nil {

            self.addSubview(advertisingView)
            advertisingView.addSubview(advertisingLabel)

            [advertisingView, nameLabel, moreButton,priceLabel,addButton].forEach { self.addSubview($0) }
            [advertisingLabel, moreButton, priceLabel].forEach { $0.sizeToFit() }

            advertisingLabel.frame.origin = CGPoint(
                x: 12,
                y: 4)

            advertisingView.frame = CGRect(
                x: 20,
                y: 20,
                width: advertisingLabel.frame.width + 24,
                height: advertisingLabel.frame.height + 8)

            nameLabel.frame = CGRect(
                x: advertisingView.frame.minX,
                y: advertisingView.frame.maxY + 12,
                width: self.frame.width - 36,
                height: labelHeight)



        } else {

            [nameLabel, moreButton,priceLabel,addButton].forEach { self.addSubview($0) }
            [moreButton, priceLabel].forEach { $0.sizeToFit() }

            nameLabel.frame = CGRect(
                x: 20,
                y: 20,
                width: self.frame.width - 36,
                height: labelHeight)
        }

        moreButton.frame.origin = CGPoint(
            x: 20,
            y: nameLabel.frame.maxY + 4)

        priceLabel.frame.origin = CGPoint(
            x: 20,
            y: moreButton.frame.maxY + 23)

        addButton.frame = CGRect(
            x: self.frame.maxX - 108,
            y: moreButton.frame.maxY + 16, width: 72, height: 36)

        let boundsHeight = addButton.frame.maxY - advertisingLabel.frame.minY + 20

        self.bounds = CGRect(
            x: 0,
            y: 0,
            width: self.frame.width,
            height: boundsHeight)
        
    }

    @objc private func didTapMoreButton() {
        didTapMoreButtonClosure?()
    }

    @objc private func didTapAddButton() {
        didTapAddButtonClosure?()
    }
    
    private func heightForLable(text: String?, font: UIFont, width: CGFloat) -> CGFloat {
        guard let text = text else {
            return 0
        }

        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()

        return label.frame.height
    }
}

