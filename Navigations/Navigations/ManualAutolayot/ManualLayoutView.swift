//
//  ManualLayoutView.swift
//  Navigations
//
//  Created by Alex on 11.03.2022.
//

import UIKit

class ManualLayoutView: UIView {

    // MARK: - Private property

    private lazy var rootScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    private lazy var contentView: UIView = {
        let view = UIView ()
        return view
    }()

    private var header: CustomHeaderViewManual = {
        let view = CustomHeaderViewManual(
            frame: .zero,
            titleText: "Курсы IT",
            imageName: "technicalSupport")
        return view
    }()

    // MARK: - Public property

    lazy var firstCard: CustomCardViewManual = {
        let view = CustomCardViewManual(
            frame: .zero,
            advertisingText: "Первые три занятия беcплатно",
            nameText: "Backend разработка",
            price: "7 840")
        return view
    }()

    lazy var secondCard: CustomCardViewManual = {
        let view = CustomCardViewManual(
            frame: .zero,
            advertisingText: "1.02.2022 - 1.04.2022 Скидка 10%",
            nameText: "Курсы по Android разроботке",
            price: "9 800")
        return view
    }()

    lazy var thirdCard: CustomCardViewManual = {
        let view = CustomCardViewManual(
            frame: .zero,
            advertisingText: nil,
            nameText: "Курсы по iOS разработке + Computer Science + Computer Vision + System Design",
            price: "11 760")
        return view
    }()

    // MARK: - Initialisation

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(rootScrollView)
        rootScrollView.addSubview(contentView)
        [header, firstCard, secondCard,thirdCard ].forEach { contentView.addSubview($0) }
        [header, firstCard, secondCard, thirdCard ].forEach { $0.setNeedsLayout()}

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureView()
    }
}

// MARK: - Private methods

extension ManualLayoutView {
    
    private func configureView() {

        rootScrollView.frame = CGRect(
            x: 0,
            y: 0,
            width: self.bounds.width,
            height:  self.bounds.height - self.safeAreaInsets.bottom)

        header.frame = CGRect(
            x: 16,
            y: 0,
            width: self.bounds.width - 32,
            height: header.bounds.height)

        firstCard.frame = CGRect(
            x: header.frame.minX,
            y: header.frame.maxY + 16,
            width: header.bounds.width,
            height: firstCard.bounds.height)

        secondCard.frame = CGRect(
            x: header.frame.minX,
            y: firstCard.frame.maxY + 16,
            width: header.bounds.width,
            height: secondCard.bounds.height)

        thirdCard.frame = CGRect(
            x: header.frame.minX,
            y: secondCard.frame.maxY + 16,
            width: header.bounds.width,
            height: thirdCard.bounds.height)


        contentView.frame = CGRect(
            x: 0,
            y: 0,
            width: self.bounds.width,
            height:  thirdCard.frame.maxY + 20 - header.frame.minY)

        rootScrollView.contentSize = CGSize(width: contentView.frame.width, height:  contentView.frame.height)
    }
}
