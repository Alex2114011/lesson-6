//
//  AppDelegate.swift
//  Navigations
//
//  Created by Alex on 04.03.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = CustomTabBarController()
        self.window = window
        window.makeKeyAndVisible()
        return true
    }
}

