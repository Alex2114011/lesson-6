//
//  CustomTabBarController.swift
//  Navigations
//
//  Created by Alex on 13.03.2022.
//

import UIKit

final class CustomTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUITabBar()
        setupViewControllers()
    }

}

// MARK: - Private methods

extension CustomTabBarController {

    private func setupUITabBar() {

        let appearance = UITabBarAppearance()
        appearance.configureWithDefaultBackground()
        appearance.backgroundColor = UIColor.red

        appearance.stackedLayoutAppearance.normal.iconColor = .white
        appearance.stackedLayoutAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.white]

        appearance.compactInlineLayoutAppearance.normal.iconColor = .cyan
        appearance.compactInlineLayoutAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.cyan]


        appearance.inlineLayoutAppearance.normal.iconColor = .black
        appearance.inlineLayoutAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.black]


        self.tabBar.standardAppearance = appearance
        self.tabBar.scrollEdgeAppearance = self.tabBar.standardAppearance
        self.tabBar.tintColor = .purple

    }

   private func setupViewControllers() {

        viewControllers = [
            createNavController(for: AutolayoutViewController(),
                                   title: "AutoLayout",
                                   image: UIImage(named: "home"),
                                   isLarge: false),
            createNavController(for: CourseViewController(),
                                   title: "XIB",
                                   image: UIImage(named: "backspace"),
                                   isLarge: false),
            createNavController(for: ManualLayoutViewController(),
                                   title: "Manual",
                                   image: UIImage(named: "block-layers"),
                                   isLarge: false),
            createNavController(for: NormalNavigation(),
                                   title: "Normal",
                                   image: UIImage(named: "startup"),
                                   isLarge: false),
            createNavController(for: LargeNavigation(),
                                   title: "Large",
                                   image: UIImage(named: "archive"),
                                   isLarge: true)
        ]
    }

    private func createNavController(for rootViewController: UIViewController,
                                     title: String,
                                     image: UIImage?,
                                     isLarge: Bool) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        navController.navigationBar.prefersLargeTitles = isLarge
        rootViewController.navigationItem.title = title

        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .red
        appearance.shadowColor = .clear
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

        navController.navigationBar.standardAppearance = appearance
        navController.navigationBar.scrollEdgeAppearance = appearance
        navController.navigationBar.compactAppearance = appearance
        return navController
    }

}

